package com.xin.usercenter.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;




/**
 * <p>
 * 
 * </p>
 *
 * @author BianPeng
 * @since 2019-04-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Permission extends Model<Permission> {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id" , value = "角色ID")
    private Long id;

    @ApiModelProperty(name = "permission" , value = "权限")
    private String permission;

    @ApiModelProperty(name = "remark" , value = "权限说明")
    private String remark;

    @ApiModelProperty(name = "state" , value = "状态")
    private Integer state;

    @ApiModelProperty(name = "createUid" , value = "创建用户ID")
    private Long createUid;

    @ApiModelProperty(name = "createTime" , value = "创建时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(name = "updateUid" , value = "更新用户ID")
    private Long updateUid;

    @ApiModelProperty(name = "updateTime" , value = "更新时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;
    
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
