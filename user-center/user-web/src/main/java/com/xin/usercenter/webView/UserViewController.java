/**
 * @filename:Const 2018年06月06日
 * @project OnlineGame    边鹏  V1.0
 * Copyright(c) 2018 BianP Co. Ltd. 
 * All right reserved. 
 */
package com.xin.usercenter.webView;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @描述 	用户相关页面请求
 * @注意		此类只能用@Controller 不能使用@RestController
 * @author  BianP
 */
@Controller  
@RequestMapping("/userapi")
public class UserViewController {
	/**
	 * @explain 用户登录《GET》
	 * @return  String
	 * @author  BianP
	 */
	@RequestMapping(value="/toLogin", method = RequestMethod.GET)
	public String toLogin(Model model){
		model.addAttribute("welcome", "欢迎登录");
		return "login";
	}
}
