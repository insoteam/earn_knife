package com.xin.dealcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.xin.usercenter.feign.api", "com.xin.dealcenter.api"})
public class DealCenterApplication {
	public static void main(String[] args) {
		SpringApplication.run(DealCenterApplication.class, args);
	}
}
