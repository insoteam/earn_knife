/**
 * @filename:Order 2018年7月5日
 * @project deal-center  V1.0
 * Copyright(c) 2018 BianP Co. Ltd. 
 * All right reserved. 
 */
package com.xin.dealcenter.entity;

import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  订单
 * @Author:       BianP   
 * @CreateDate:   2018年7月5日
 * @Version:      V1.0
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {

	private static final long serialVersionUID = 1531104207412L;
	
	@ApiModelProperty(name = "id" , value = "ID")
	private Long id;
	@ApiModelProperty(name = "orderNo" , value = "订单编号")
	private String orderNo;
	@ApiModelProperty(name = "uid" , value = "用户ID")
	private Long uid;
	@ApiModelProperty(name = "source" , value = "来源")
	private String source;
	@ApiModelProperty(name = "productId" , value = "产品ID")
	private Long productId;
	@ApiModelProperty(name = "productName" , value = "产品名字")
	private String productName;
	@ApiModelProperty(name = "unitPrice" , value = "单价")
	private Integer unitPrice;
	@ApiModelProperty(name = "number" , value = "数量")
	private Integer number;
	@ApiModelProperty(name = "sellingPrice" , value = "卖价")
	private Integer sellingPrice;
	@ApiModelProperty(name = "state" , value = "0等待支付，1支付成功，2支付失败，3撤销")
	private Integer state;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "createTime" , value = "创建时间")
	private Date createTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "updateTime" , value = "交易变化时间")
	private Date updateTime;
}
